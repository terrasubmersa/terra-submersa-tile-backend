package ch.octo.terrasubmersa.coordinates

import ch.octo.terrasubmersa.tiles.slippy.SlippyTile
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.TableDrivenPropertyChecks._

/**
  * Created by alex on 21.08.17.
  */
class LatLonCoordsTest extends FlatSpec with Matchers {
  "point " should "convert to tile fraction" in {
    val ll = LatLonCoords(41.26000108568697, -95.93965530395508)
    val xy = ll.toTileFraction(9)
    xy.x should equal(119.552 +- 0.001)
    xy.y should equal(191.471 +- 0.001)
  }
  "toTile" should "9/41.2600/-95.9396 -> (119, 191, 9)" in {
    LatLonCoords(41.2600, -95.9396).toTile(9) should equal(SlippyTile(119, 191, 9))
  }

  val checkDegFromString = Table(
    ("input", "expected"),
    (" 62.234 ", 62.234),
    ("54° 0' 0\"", 54.0),
    (" 037� 25' 36.3970\" ", 37.426781)
  )

  forAll(checkDegFromString){
    (givenInut:String, thenDeg:Double) =>
      s"""LatLonCoords.degFromString("$givenInut")""" should s"$thenDeg" in {
        LatLonCoords.degFromString(givenInut) should be(thenDeg +- 0.0001)

      }
  }

  "plusHoriz(meter)" should "give a new coordinate" in {
    val ll = LatLonCoords(41.26000108568697, -95.93965530395508)
    val ll2 = ll.plusHoriz(Meter(-20000))
    ll2.lat should equal(ll.lat)
    ll2.lon should equal(-96.179003 +- 0.0001)
  }
  "plusVert(meter)" should "give a new coordinate" in {
    val ll = LatLonCoords(41.26000108568697, -95.93965530395508)
    val ll2 = ll.plusVert(Meter(20000))
    ll2.lon should equal(ll.lon)
    ll2.lat should equal(41.439925 +- 0.0001)
  }
}
