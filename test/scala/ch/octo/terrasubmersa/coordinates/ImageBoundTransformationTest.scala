package ch.octo.terrasubmersa.coordinates

import ch.octo.terrasubmersa.coordinates
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.TableDrivenPropertyChecks._

class ImageBoundTransformationTest extends FlatSpec with Matchers {
  val checksRelativeCoordinates =
    Table(
      ("givenCenterLat", "givenCenterLon", "givenWidthLat", "givenWidthLon", "givenRotation", "whenRelLat", "whenRelLon", "thenLat", "thenLon"),
      (60.0, 30.0, 40.0, 20.0, 0.0, 0.0, 0.0, 60.0, 30.0),
      (60.0, 30.0, 40.0, 20.0, 0.0, -1.0, 0.0, 40.0, 30.0),
      (60.0, 30.0, 40.0, 20.0, 0.0, +1.0, 0.0, 80.0, 30.0),
      (60.0, 30.0, 40.0, 20.0, 0.0, 0.0, -1.0, 60.0, 20.0),
      (60.0, 30.0, 40.0, 20.0, 0.0, 0.0, +1.0, 60.0, 40.0),
      (60.0, 30.0, 40.0, 20.0, -30.0, 0.0, 0.0, 60.0, 30.0),
      (60.0, 30.0, 40.0, 20.0, -30.0, 1.0, 0.0, 77.32050807568878, 40.0),
      (60.0, 30.0, 40.0, 20.0, -30.0, -1.0, 1.0, 37.67949192431122, 28.66025403784439),
      (60.0, 30.0, 40.0, 20.0, -30.0, -1.0, -1.0, 47.67949192431122, 11.339745962155613)

    )

  behavior of "ImageTransformationTest"

  forAll(checksRelativeCoordinates) {
    (givenCenterLat: Double,
     givenCenterLon: Double,
     givenWidthLat: Double,
     givenWidthLon: Double,
     givenRotation: Double,
     whenRelLat: Double,
     whenRelLon: Double,
     thenLat: Double,
     thenLon: Double
    ) => {
      it should s"relativeCoordinates $givenRotation° ($thenLat, $thenLon)" in {
        val box = ImageBoundTransformation(
          new LatLonCoords(givenCenterLat, givenCenterLon),
          givenWidthLat,
          givenWidthLon,
          givenRotation
        )
        box.relativeCoordinates(whenRelLat, whenRelLon) should be(LatLonCoords(thenLat, thenLon))
      }
    }
  }
  val checksRBBox =
    Table(
      ("givenCenterLat", "givenCenterLon", "givenWidthLat", "givenWidthLon", "givenRotation", "thenULLat", "thenULLon", "thenLRLat", "thenLRLon"),
      (60.0, 30.0, 40.0, 20.0, 0.0, 80.0, 20.0, 40.0, 40.0),
        (60.0, 30.0, 40.0, 20.0, -30.0, 82.32050807568878, 11.339745962155613, 37.67949192431122, 48.66025403784439)
    )
  forAll(checksRBBox) {
    (givenCenterLat: Double,
     givenCenterLon: Double,
     givenWidthLat: Double,
     givenWidthLon: Double,
     givenRotation: Double,
     thenULLat: Double,
     thenULLon: Double,
     thenLRLat: Double,
     thenLRLon: Double
    ) => {
      val box = ImageBoundTransformation(
        new LatLonCoords(givenCenterLat, givenCenterLon),
        givenWidthLat,
        givenWidthLon,
        givenRotation
      )

      it should s"toBBox $box" in {
        box.toBBox should equal(LatLonBBox(
          LatLonCoords(thenULLat, thenULLon),
          LatLonCoords(thenLRLat, thenLRLon)
        ))
      }
    }
  }

}
