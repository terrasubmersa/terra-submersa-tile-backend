package ch.octo.terrasubmersa.tiles.image

import org.scalatest.matchers.{BeMatcher, MatchResult}
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by alex on 23.08.17.
  */
class ImageTransfoTest extends FlatSpec with Matchers {
  "equal" should "exact" in {
    ImageTransfo(0, 1, 0.33, 3, 4) should equal(ImageTransfo(0, 1, 0.33, 3, 4))
  }
  "equal" should "almost scale" in {
    ImageTransfo(0, 1, 0.33, 3, 4) should equal(ImageTransfo(0, 1, 0.3300001, 3, 4))
  }
  "equal" should "not crop0" in {
    ImageTransfo(0, 1, 0.33, 3, 4) should not equal (ImageTransfo(10, 1, 0.33, 3, 4))
  }
  "equal" should "not crop1" in {
    ImageTransfo(0, 1, 0.33, 3, 4) should not equal (ImageTransfo(0, 10, 0.33, 3, 4))
  }
  "equal" should "not scale" in {
    ImageTransfo(0, 1, 0.33, 3, 4) should not equal (ImageTransfo(0, 1, 0.35, 3, 4))
  }
  "equal" should "not pad0" in {
    ImageTransfo(0, 1, 0.33, 3, 4) should not equal (ImageTransfo(0, 1, 0.33, 30, 4))
  }
  "equal" should "not pad1" in {
    ImageTransfo(0, 1, 0.33, 3, 4) should not equal (ImageTransfo(0, 1, 0.33, 3, 40))
  }


  "ImageTransfo.build" should "identity" in {
    ImageTransfo.build(1000, 2000, 1000, 2000, 100, 100) should equal(ImageTransfo(0, 0, 1, 0, 0))
  }
  "ImageTransfo.build" should "size up" in {
    ImageTransfo.build(1000, 2000, 1000, 2000, 200, 100) should equal(ImageTransfo(0, 0, 2, 0, 0))
  }
  "ImageTransfo.build" should "size down" in {
    ImageTransfo.build(1000, 2000, 1000, 2000, 100, 200) should equal(ImageTransfo(0, 0, 0.5, 0, 0))
  }
  "ImageTransfo.build" should "image is outer left" in {
    ImageTransfo.build(1000, 2000, 500, 2000, 100, 100) should equal(ImageTransfo(33, 0, 1.5, 0, 0))
  }
  "ImageTransfo.build" should "image is outer right" in {
    ImageTransfo.build(1000, 2000, 1000, 2500, 100, 100) should equal(ImageTransfo(0, 33, 1.5, 0, 0))
  }
  "ImageTransfo.build" should "image is outer left right and scale)" in {
    ImageTransfo.build(1000, 2000, 500, 2500, 100, 300) should equal(ImageTransfo(75, 75, 1 / 1.5, 0, 0))
  }
  "ImageTransfo.build" should "image is inner left/right" in {
    ImageTransfo.build(1000, 2000, 1250, 1750, 100, 100) should equal(ImageTransfo(0, 0, 0.5, 25, 25))
  }
  "ImageTransfo.build" should "image is inner left/right, but already right size" in {
    ImageTransfo.build(1000, 2000, 1250, 1750, 100, 50) should equal(ImageTransfo(0, 0, 1.0, 25, 25))
  }
  "ImageTransfo.build" should "(image is inner left/right and scale)" in {
    ImageTransfo.build(1000, 2000, 1250, 1750, 100, 300) should equal(ImageTransfo(0, 0, 1 / 6.0, 25, 25))
  }
  "ImageTransfo.build" should "14/9243/6350 lon" in {
    ImageTransfo.build(23.09326171875, 23.11523438, 23.09417842, 23.14084353, 256, 20402) should
      equal(ImageTransfo(0, 11196, 0.026649, 11, 0))
  }
  "ImageTransfo.build" should "14/9243/6350 lat" in {
    ImageTransfo.build(37.4574181, 37.47485808, 37.39003124, 37.46029356, 256, 30720) should
      equal(ImageTransfo(29463, 0, 0.033573, 0, 214))
  }
}
