package ch.octo.terrasubmersa.tiles.slippy

import ch.octo.terrasubmersa.coordinates.HasLatLonBBox
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable

/**
  * Created by alex on 22.08.17.
  */
class SlippyTileNodeTest extends FlatSpec with Matchers {
  "just end tile" should "not have children" in {
    val t = SlippyTileNode.generateTree(SlippyTile(23, 45, 6), 6)
    t should equal(SlippyTileNode(SlippyTile(23, 45, 6), Set.empty))
  }
  "two more levels" should "21 children" in {
    val t = SlippyTileNode.generateTree(SlippyTile(119, 191, 9), 11)
    t should equal(SlippyTileNode(SlippyTile(119, 191, 9),
      Set(
        SlippyTileNode(SlippyTile(238, 382, 10),
          Set(
            SlippyTileNode(SlippyTile(476, 764, 11), Set.empty),
            SlippyTileNode(SlippyTile(477, 764, 11), Set.empty),
            SlippyTileNode(SlippyTile(476, 765, 11), Set.empty),
            SlippyTileNode(SlippyTile(477, 765, 11), Set.empty)
          )
        ),
        SlippyTileNode(SlippyTile(239, 382, 10),
          Set(
            SlippyTileNode(SlippyTile(478, 764, 11), Set.empty),
            SlippyTileNode(SlippyTile(479, 764, 11), Set.empty),
            SlippyTileNode(SlippyTile(478, 765, 11), Set.empty),
            SlippyTileNode(SlippyTile(479, 765, 11), Set.empty)
          )
        ),
        SlippyTileNode(SlippyTile(238, 383, 10),
          Set(
            SlippyTileNode(SlippyTile(476, 766, 11), Set.empty),
            SlippyTileNode(SlippyTile(477, 766, 11), Set.empty),
            SlippyTileNode(SlippyTile(476, 767, 11), Set.empty),
            SlippyTileNode(SlippyTile(477, 767, 11), Set.empty)
          )
        ),
        SlippyTileNode(SlippyTile(239, 383, 10),
          Set(
            SlippyTileNode(SlippyTile(478, 766, 11), Set.empty),
            SlippyTileNode(SlippyTile(479, 766, 11), Set.empty),
            SlippyTileNode(SlippyTile(478, 767, 11), Set.empty),
            SlippyTileNode(SlippyTile(479, 767, 11), Set.empty)
          )
        )
      )
    ))
  }
  "two more levels, filtered on even sum x+y" should "7 children" in {
    //dummy filter on keep HasLatLonBBox where the corresponding tile x and y sum up to an even number
    def dummyFilter(u: HasLatLonBBox): Boolean = {
      val st = u.toBBox.minimumOverlayTile().get
      (st.x + st.y) % 2 == 0
    }

    val t = SlippyTileNode.generateTree(SlippyTile(119, 191, 9), 11, dummyFilter)
    t should equal(SlippyTileNode(SlippyTile(119, 191, 9),
      Set(
        SlippyTileNode(SlippyTile(238, 382, 10),
          Set(
            SlippyTileNode(SlippyTile(476, 764, 11), Set.empty),
            SlippyTileNode(SlippyTile(477, 765, 11), Set.empty)
          )
        ),
        SlippyTileNode(SlippyTile(239, 383, 10),
          Set(
            SlippyTileNode(SlippyTile(478, 766, 11), Set.empty),
            SlippyTileNode(SlippyTile(479, 767, 11), Set.empty)
          )
        )
      )
    ))
  }
  "WalkDepthFirst" should "go through all nodes" in {
    val t = SlippyTileNode.generateTree(SlippyTile(119, 191, 9), 11)
    val lCollect = mutable.Set[(Int, Int)]()

    t.walkDepthFirst((tile: SlippyTile) => {
      lCollect += ((tile.x, tile.y))
    })
    lCollect should equal(mutable.Set(
      (119, 191),
      (238, 382),
      (476, 764),
      (477, 764),
      (476, 765),
      (477, 765),
      (239, 382),
      (478, 764),
      (479, 764),
      (478, 765),
      (479, 765),
      (238, 383),
      (476, 766),
      (477, 766),
      (476, 767),
      (477, 767),
      (239, 383),
      (478, 766),
      (479, 766),
      (478, 767),
      (479, 767)
    ))
  }

  "walkAndDo" should "go through all nodes" in {
    val lCollect = mutable.Set[(Int, Int)]()
    val t = SlippyTileNode.walkAndDo(
      SlippyTile(119, 191, 9),
      11,
      (tile: HasLatLonBBox)=>true,
      (tile: SlippyTile) => lCollect += ((tile.x, tile.y)))

    lCollect should equal(mutable.Set(
      (119, 191),
      (238, 382),
      (476, 764),
      (477, 764),
      (476, 765),
      (477, 765),
      (239, 382),
      (478, 764),
      (479, 764),
      (478, 765),
      (479, 765),
      (238, 383),
      (476, 766),
      (477, 766),
      (476, 767),
      (477, 767),
      (239, 383),
      (478, 766),
      (479, 766),
      (478, 767),
      (479, 767)
    ))
  }
}
