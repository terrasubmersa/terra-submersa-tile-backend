package ch.octo.terrasubmersa.tiles.bathymetry

import java.io.{File, PrintWriter}

import ch.octo.terrasubmersa.bathymetry.BathymetryCell
import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords}
import ch.octo.terrasubmersa.tiles.io.reader.AscBathyMetryGridReader
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by alex on 11.09.17.
  */
class ContourTest extends FlatSpec with Matchers {
  val bb = LatLonBBox(LatLonCoords(12, 20), LatLonCoords(10, 22))
  "Contour.getUnit" should "Nil when not all cells are defined" in {
    Contour.getUnit(BathymetryCell(None), BathymetryCell(Some(1)), BathymetryCell(Some(1.8)), BathymetryCell(Some(1.5)), 2.0, bb) should
      equal(Nil)

  }
  "Contour.getUnit" should "Nil when all below" in {
    Contour.getUnit(BathymetryCell(Some(0.9)), BathymetryCell(Some(1)), BathymetryCell(Some(1.8)), BathymetryCell(Some(1.5)), 2.0, bb) should
      equal(Nil)
  }
  "Contour.getUnit" should "Nil when all above" in {
    Contour.getUnit(BathymetryCell(Some(0.9)), BathymetryCell(Some(1)), BathymetryCell(Some(1.8)), BathymetryCell(Some(1.5)), 0.5, bb) should
      equal(Nil)
  }
  """Contour.getUnit case 1
    |--
    |+-
  """.stripMargin should
    """...
      |...
      |\..
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(0.9)), BathymetryCell(Some(1)), BathymetryCell(Some(2.8)), BathymetryCell(Some(1.5)), 2, bb) should
      equal(List(ContourUnit(11, 20, 10, 21)))
  }
  """Contour.getUnit case 14
    |++
    |-+
  """.stripMargin should
    """...
      |...
      |\..
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(2.9)), BathymetryCell(Some(2.5)), BathymetryCell(Some(1.8)), BathymetryCell(Some(2.5)), 2, bb) should
      equal(List(ContourUnit(11, 20, 10, 21)))
  }

  """Contour.getUnit case 2
    |--
    |-+
  """.stripMargin should
    """...
      |...
      |../
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(0.9)), BathymetryCell(Some(1)), BathymetryCell(Some(1.8)), BathymetryCell(Some(2.5)), 2, bb) should
      equal(List(ContourUnit(10, 21, 11, 22)))
  }
  """Contour.getUnit case 13
    |++
    |+-
  """.stripMargin should
    """...
      |...
      |../
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(2.9)), BathymetryCell(Some(2.5)), BathymetryCell(Some(2.8)), BathymetryCell(Some(1.5)), 2, bb) should
      equal(List(ContourUnit(10, 21, 11, 22)))
  }
  """Contour.getUnit case 3
    |--
    |++
  """.stripMargin should
    """...
      |---
      |../
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(0.9)), BathymetryCell(Some(1)), BathymetryCell(Some(2.8)), BathymetryCell(Some(2.5)), 2, bb) should
      equal(List(ContourUnit(11, 20, 11, 22)))
  }
  """Contour.getUnit case 12
    |++
    |--
  """.stripMargin should
    """...
      |---
      |...
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(2.9)), BathymetryCell(Some(2.1)), BathymetryCell(Some(1.8)), BathymetryCell(Some(1.5)), 2, bb) should
      equal(List(ContourUnit(11, 20, 11, 22)))
  }
  """Contour.getUnit case 4
    |-+
    |--
  """.stripMargin should
    """..\
      |...
      |...
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(1.9)), BathymetryCell(Some(2.1)), BathymetryCell(Some(1.8)), BathymetryCell(Some(1.5)), 2, bb) should
      equal(List(ContourUnit(12, 21, 11, 22)))
  }
  """Contour.getUnit case 11
    |+-
    |++
  """.stripMargin should
    """..\
      |...
      |...
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(2.9)), BathymetryCell(Some(1.1)), BathymetryCell(Some(2.8)), BathymetryCell(Some(2.5)), 2, bb) should
      equal(List(ContourUnit(12, 21, 11, 22)))
  }
  """Contour.getUnit case 5
    |-+
    |+-
  """.stripMargin should
    """/..
      |...
      |../
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(1.9)), BathymetryCell(Some(2.1)), BathymetryCell(Some(2.8)), BathymetryCell(Some(1.5)), 2, bb) should
      equal(List(ContourUnit(11, 20, 12, 21), ContourUnit(10, 21, 11, 22)))
  }

  """Contour.getUnit case 6
    |-+
    |-+
  """.stripMargin should
    """.|.
      |.|.
      |.|.
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(1.9)), BathymetryCell(Some(2.1)), BathymetryCell(Some(1.8)), BathymetryCell(Some(2.5)), 2, bb) should
      equal(List(ContourUnit(10, 21, 12, 21)))
  }
  """Contour.getUnit case 9
    |+-
    |+-
  """.stripMargin should
    """.|.
      |.|.
      |.|.
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(2.9)), BathymetryCell(Some(1.1)), BathymetryCell(Some(2.8)), BathymetryCell(Some(1.5)), 2, bb) should
      equal(List(ContourUnit(10, 21, 12, 21)))
  }
  """Contour.getUnit case 7
    |-+
    |++
  """.stripMargin should
    """/..
      |...
      |...
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(1.9)), BathymetryCell(Some(2.1)), BathymetryCell(Some(2.8)), BathymetryCell(Some(2.5)), 2, bb) should
      equal(List(ContourUnit(11, 20, 12, 21)))
  }
  """Contour.getUnit case 8
    |+-
    |--
  """.stripMargin should
    """/..
      |...
      |...
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(2.9)), BathymetryCell(Some(1.1)), BathymetryCell(Some(1.8)), BathymetryCell(Some(1.5)), 2, bb) should
      equal(List(ContourUnit(11, 20, 12, 21)))
  }
  """Contour.getUnit case 8
    |+-
    |-+
  """.stripMargin should
    """..\
      |...
      |\..
    """.stripMargin in {
    Contour.getUnit(BathymetryCell(Some(2.9)), BathymetryCell(Some(1.1)), BathymetryCell(Some(1.8)), BathymetryCell(Some(2.5)), 2, bb) should
      equal(List(ContourUnit(11, 20, 10, 21), ContourUnit(12, 21, 11, 22)))
  }

  //values themselves where saved to file plot to R are the results compared
  "contour" should "extract all pieces from file" in {
    val grid = AscBathyMetryGridReader.load("test/resources/simple_asc.utm64_34_contour.asc")
    val us = Contour.build(grid, -2)
    us.units.toSet should equal(Set(ContourUnit(37.110802059314544, 23.029525471586254, 37.11791846771449, 23.038022225434126), ContourUnit(37.11791846771449, 23.038022225434126, 37.11791846771449, 23.05501573312987), ContourUnit(37.11791846771449, 23.05501573312987, 37.110802059314544, 23.063512486977743), ContourUnit(37.09656924251464, 23.012531963890517, 37.10368565091459, 23.021028717738385), ContourUnit(37.10368565091459, 23.021028717738385, 37.110802059314544, 23.029525471586254), ContourUnit(37.110802059314544, 23.063512486977743, 37.10368565091459, 23.07200924082561), ContourUnit(37.10368565091459, 23.07200924082561, 37.10368565091459, 23.089002748521352), ContourUnit(37.10368565091459, 23.089002748521352, 37.09656924251464, 23.097499502369224), ContourUnit(37.08945283411469, 23.004035210042645, 37.09656924251464, 23.012531963890517), ContourUnit(37.09656924251464, 23.097499502369224, 37.08945283411469, 23.105996256217093), ContourUnit(37.08945283411469, 23.105996256217093, 37.08945283411469, 23.122989763912834), ContourUnit(37.08945283411469, 23.122989763912834, 37.082336425714736, 23.131486517760706), ContourUnit(37.09656924251464, 23.14848002545645, 37.08945283411469, 23.15697677930432), ContourUnit(37.082336425714736, 23.131486517760706, 37.075220017314784, 23.13998327160858), ContourUnit(37.075220017314784, 23.13998327160858, 37.06810360891483, 23.14848002545645), ContourUnit(37.05387079211493, 23.097499502369224, 37.06098720051488, 23.105996256217093), ContourUnit(37.06098720051488, 23.105996256217093, 37.06098720051488, 23.122989763912834), ContourUnit(37.06098720051488, 23.122989763912834, 37.06098720051488, 23.13998327160858), ContourUnit(37.06098720051488, 23.13998327160858, 37.06810360891483, 23.14848002545645), ContourUnit(37.05387079211493, 23.14848002545645, 37.06098720051488, 23.15697677930432), ContourUnit(37.039637975315024, 23.097499502369224, 37.05387079211493, 23.097499502369224), ContourUnit(37.05387079211493, 23.14848002545645, 37.046754383714976, 23.15697677930432), ContourUnit(37.02540515851513, 23.097499502369224, 37.039637975315024, 23.097499502369224), ContourUnit(37.02540515851513, 23.11449301006496, 37.03252156691508, 23.122989763912834), ContourUnit(37.03252156691508, 23.122989763912834, 37.03252156691508, 23.13998327160858), ContourUnit(37.03252156691508, 23.13998327160858, 37.02540515851513, 23.14848002545645), ContourUnit(37.01117234171522, 23.097499502369224, 37.02540515851513, 23.097499502369224), ContourUnit(37.02540515851513, 23.11449301006496, 37.018288750115175, 23.122989763912834), ContourUnit(37.018288750115175, 23.122989763912834, 37.01117234171522, 23.131486517760706), ContourUnit(37.02540515851513, 23.14848002545645, 37.018288750115175, 23.15697677930432)))

    /* uncomment to save file and execute R script
       val pw = new PrintWriter(new File("/tmp/a.txt" ))
       pw.write("Hello, world")
       pw.write("x0 y0 x1 xy\n")
       us.units.foreach(u => pw.write(s"${u.lonFrom} ${u.latFrom} ${u.lonTo} ${u.latTo}\n"))
       pw.close
       println("""library(ggplot2)
                 |dt = read.delim('/tmp/a.txt', sep=' ')
                 |ggplot(dt) + geom_segment(aes(x=x0, y=y0, xend=x1, yend=y1))
                 |""".stripMargin)
   */
  }
}

