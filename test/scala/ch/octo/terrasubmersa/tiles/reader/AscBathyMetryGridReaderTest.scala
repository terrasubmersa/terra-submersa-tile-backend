package ch.octo.terrasubmersa.tiles.io.reader

import ch.octo.terrasubmersa.bathymetry.BathymetryCell
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by alex on 09.09.17.
  */
class AscBathyMetryGridReaderTest extends FlatSpec with Matchers {
  "AscBathyMetryGridReader.load" should "throw if wrong header" in {
    intercept[AscBathyMetryGridReader.HeaderNotFoundValueException] {
      AscBathyMetryGridReader.load("test/resources/simple_asc.utm64_34_bad_header.asc")
    }
  }

  "AscBathyMetryGridReader.load" should "extract header" in {
    val grid = AscBathyMetryGridReader.load("test/resources/simple_asc.utm64_34.asc")
    grid.ncols should equal(4)
    grid.nrows should equal(3)
    grid.boundaries.upperLeft.lat should equal(37.3991838 +- 0.00001)
    grid.boundaries.upperLeft.lon should equal(23.09982460 +- 0.00001)
    grid.boundaries.lowerRight.lat should equal(37.3991537584 +- 0.00001)
    grid.boundaries.lowerRight.lon should equal(23.09988225901168 +- 0.00001)
  }
  "AscBathyMetryGridReader.load" should "get min/max depth" in {
    val grid = AscBathyMetryGridReader.load("test/resources/simple_asc.utm64_34.asc")
    grid.minDepth should equal(-3.4)
    grid.maxDepth should equal(-1.2)

  }
  "AscBathyMetryGridReader.load" should "rad values" in {
    val grid = AscBathyMetryGridReader.load("test/resources/simple_asc.utm64_34.asc")
    grid.cells should equal(
      List(
        List(BathymetryCell(None), BathymetryCell(Some(-1.2)), BathymetryCell(Some(-1.3)), BathymetryCell(None)),
        List(BathymetryCell(Some(-2.1)), BathymetryCell(Some(-2.2)), BathymetryCell(Some(-2.3)), BathymetryCell(None)),
        List(BathymetryCell(None), BathymetryCell(None), BathymetryCell(Some(-3.3)), BathymetryCell(Some(-3.4)))
      )
    )
  }
//  "prout" should "paf" in {
//    val grid = AscBathyMetryGridReader.load("/Users/alex/Downloads/koilada_portoheli/koilada_utm34n.asc")
//    println(s"boundaries: ${grid.boundaries}")
//  }
}
