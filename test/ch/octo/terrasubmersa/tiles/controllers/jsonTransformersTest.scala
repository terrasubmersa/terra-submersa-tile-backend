package ch.octo.terrasubmersa.tiles.controllers

import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords}
import ch.octo.terrasubmersa.tiles.controllers.JsonTransformers._
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemBoundaries}
import org.scalatest.{FlatSpec, Matchers}
import play.api.libs.json._


class jsonTransformersTest extends FlatSpec with Matchers {

  behavior of "jsonTransformersTest"
  private val objUpperLeft = LatLonCoords(12.34, 56.78)
  private val objLowerRight = LatLonCoords(-12.34, 156.78)
  private val strUpperLeft = """{"lat":12.34, "lon":56.78 } """
  private val strLowerRight = """{"lat":-12.34, "lon":156.78 } """
  private val jsUpperLeft = JsObject(Seq("lat" -> JsNumber(12.34), "lon" -> JsNumber(56.78)))
  private val jsLowerRight = JsObject(Seq("lat" -> JsNumber(-12.34), "lon" -> JsNumber(156.78)))

  private val objLatLonBBox = LatLonBBox(objUpperLeft, LatLonCoords(-12.34, 156.78))
  private val strLatLonBBox =
    s"""{"upperLeft" : $strUpperLeft,"lowerRight" : $strLowerRight}"""
  private val jsLatLonBBox = JsObject(Seq("upperLeft"->jsUpperLeft, "lowerRight"->jsLowerRight))


  private val objTileSystemBoundaries = TileSystemBoundaries(12, 19, objLatLonBBox)
  private val strTileSystemBoundaries =
    s"""{"minLevel":12,"maxLevel":19,"bbox":$strLatLonBBox }"""
  val jsTileSystemBoundaries = JsObject(Seq(
    "minLevel" -> JsNumber(12),
    "maxLevel" -> JsNumber(19),
    "bbox" -> jsLatLonBBox
  ))

  private val objTileSystem = TileSystem(
    "abc",
    objTileSystemBoundaries,
    "http://aaa/{z}/{x}/{y}",
    "WTF",
    "paf"
  )
  private val strTileSystem= s"""{
                                 |"id":"abc",
                                 |"boundaries":$strTileSystemBoundaries,
                                 |"urlMask":"http://aaa/{z}/{x}/{y}",
                                 |"description": "WTF",
                                 |"copyright": "paf"
                                 |}""".stripMargin
  private val jsTileSystem = JsObject(Seq(
    "id" -> JsString("abc"),
    "boundaries" -> jsTileSystemBoundaries,
    "urlMask" -> JsString("http://aaa/{z}/{x}/{y}"),
    "description" -> JsString("WTF"),
    "copyright" -> JsString("paf"),

  ))


  it should "LatLonCoordsReads" in {
    val jsonValue: JsValue = Json.parse(strUpperLeft)

    val llRes = Json.fromJson[LatLonCoords](jsonValue)

    llRes should be(JsSuccess(objUpperLeft))

  }
  it should "LatLonCoordsWrites" in {
    val js = Json.toJson(objUpperLeft)

    js should equal(jsUpperLeft)
  }


  it should "LatLonBBoxReads" in {
    val jsonValue: JsValue = Json.parse( strLatLonBBox)

    val res = Json.fromJson[LatLonBBox](jsonValue)

    res should be(JsSuccess(objLatLonBBox))
  }
  it should "LatLonBBoxWrites" in {
    val js = Json.toJson(objLatLonBBox)

    js should equal(jsLatLonBBox)
  }

  it should "TileSystemBoundariesReads" in {
    val jsonValue: JsValue = Json.parse( strTileSystemBoundaries)

    val res = Json.fromJson[TileSystemBoundaries](jsonValue)

    res should be(JsSuccess(objTileSystemBoundaries))

  }
  it should "TileSystemBoundariesWrites" in {
    val js = Json.toJson(objTileSystemBoundaries)

    js should equal(jsTileSystemBoundaries)

  }
  it should "TileSystemReads" in {
    val jsonValue: JsValue = Json.parse(strTileSystem)
    println(strTileSystem)

    val res = Json.fromJson[TileSystem](jsonValue)

    res should be(JsSuccess(objTileSystem))
  }
  it should "TileSystemWrites" in {
    val js = Json.toJson(objTileSystem)

    js should equal(jsTileSystem)

  }
}
