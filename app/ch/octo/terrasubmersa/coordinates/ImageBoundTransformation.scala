package ch.octo.terrasubmersa.coordinates

case class ImageBoundTransformation(center: LatLonCoords, widthLat: Double, widthLon: Double, rotation: Double)
  extends HasLatLonBBox {
  //  override val toBBox = ???

  /**
    * return a function taking two double and return a LatLonCoords.
    * The idea is to only compute sinus/cosinus at once
    * we have a relative referential
    * (0,0) => center
    * (+1, -1) => upper left corner
    * (-1, +1) => lower right corner
    *
    * between -1 and +1 on the y axis to remain within the box
    *
    * @return a function transforming a coordinate in a rectified system referential
    */
  val relativeCoordinates: (Double, Double) => LatLonCoords = {
    rotation match {
      case 0 => (rLat: Double, rLon: Double) => {
        val myLat = center.lat + rLat * widthLat / 2
        val myLon = center.lon + rLon * widthLon / 2
        LatLonCoords(myLat, myLon)
      }
      case r =>
        val alphaRad = Math.PI * rotation / 180
        val sin = Math.sin(alphaRad)
        val cos = Math.cos(alphaRad)
        (rLat: Double, rLon: Double) => {
          val myLat = center.lat + rLat * widthLat / 2
          val myLon = center.lon + rLon * widthLon / 2
          LatLonCoords(
            center.lat + (myLon - center.lon) * sin + (myLat - center.lat) * cos,
            center.lon + (myLon - center.lon) * cos - (myLat - center.lat) * sin
          )
        }

    }
  }
  override val toBBox = {
    val corners = List(
      relativeCoordinates(1, -1),
      relativeCoordinates(-1, 1),
      relativeCoordinates(-1, -1),
      relativeCoordinates(1, 1)
    )
    val latMax =corners.map(_.lat).max
    val latMin =corners.map(_.lat).min
    val lonMax =corners.map(_.lon).max
    val lonMin =corners.map(_.lon).min
    LatLonBBox(
      LatLonCoords(latMax, lonMin),
      LatLonCoords(latMin, lonMax)
    )
  }

}
