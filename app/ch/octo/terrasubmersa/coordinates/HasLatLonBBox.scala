package ch.octo.terrasubmersa.coordinates

/**
  * jut somethin gthat can be converted to a LatLongBBox
  * Created by alex on 22.08.17.
  */
trait HasLatLonBBox {
  val toBBox: LatLonBBox
}
