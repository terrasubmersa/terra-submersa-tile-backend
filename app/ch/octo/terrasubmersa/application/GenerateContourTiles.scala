package ch.octo.terrasubmersa.application

import java.awt.image.BufferedImage
import java.awt.{BasicStroke, Color}
import javax.imageio.ImageIO

import ch.octo.terrasubmersa.tiles.bathymetry.Contour
import ch.octo.terrasubmersa.coordinates.{ImageBoundTransformation, LatLonCoords}
import ch.octo.terrasubmersa.tiles.image.ImageBBox
import ch.octo.terrasubmersa.tiles.io.reader.AscBathyMetryGridReader
import ch.octo.terrasubmersa.tiles.slippy.SlippyTileNode
import com.typesafe.scalalogging.Logger
import org.apache.commons.io.FileUtils

/**
  * Created by alex on 22.08.17.
  */
object GenerateContourTiles extends App {
  val logger = Logger(GenerateContourTiles.getClass.getName)
  //def filename = "src/test/resources/simple_asc.utm64_34_contour.asc"
  val filename = "/Users/alex/Downloads/koilada_portoheli/koilada_utm34n.asc"
  //val filename = "src/test/resources/kilada_utm34n_16-36978-25412.asc"
  val dir = "/tmp/ts/contours/heli11"

  //taken from hand adjustment and console.log
  val transfo = ImageBoundTransformation(
    LatLonCoords(37.42511802610725, 23.11754227508778),
    0.05270356434241563,
    0.03364070302360744,
    -1.9205304149956683
  )

  // to check is a given depth is to be plot for a given tile level
  def isContourDepthForTile(tileLevel: Int, depth: Double): Boolean = tileLevel match {
    case l if l <= 10 =>
      depth.toInt % 10 == 0
    case l if l <= 12 =>
      depth.toInt % 5 == 0
    case l if l <= 14 =>
      depth.toInt % 2 == 0
    case _ => true
  }

  logger.info(s"loading $filename")
  val grid = AscBathyMetryGridReader.load(filename)

  //build contours for all integer values included betwwe min/max grid depth
  val depths = grid.cells.flatten.filter(x => x.oVa.isDefined).map(_.oVa.get)
  val depthRange = (math.ceil(depths.min).toInt to math.floor(depths.max).toInt).toList

  logger.info(s"boundaries=${grid.boundaries}")
  logger.info(s"computing contours")
  val contours = depthRange.par.map(level => {
    logger.debug(s"computing level=$level")
    Contour.build(grid, level,
      Some(
        (irow: Int, icol: Int) => {
          transfo.relativeCoordinates(
            1 - 2.0 * irow / grid.nrows,
            -1 + 2.0 * icol / grid.ncols
          )
        }
      ))
  }).toList

  //  logger.info("writing snapshot zebra")
  //  BathymetryGridSnapshotWriter.saveZebra(grid, "/tmp/koilada_utm34n")
  //  logger.info("writing snapshot contours")
  //  BathymetryGridSnapshotWriter.saveContours(grid, contours, "/tmp/koilada_utm34n")

  logger.info("writing tiles")
  val maxZoom = 19
  val bbox = transfo.toBBox
  val initTile = bbox.minimumOverlayTile().get
  SlippyTileNode.walkAndDo(initTile,
    maxZoom,
    (tile) => bbox.isIntersect(tile.toBBox),
    (tile) => {
      val canvas = new BufferedImage(ImageBBox.TILE_SIZE, ImageBBox.TILE_SIZE, BufferedImage.TYPE_INT_ARGB)
      val canvasBoundaries = grid.boundaries
      val g = canvas.createGraphics()
      g.setColor(Color.darkGray)

      def lat2pix(l: Double) = math.round(ImageBBox.TILE_SIZE * (l - tile.toBBox.upperLeft.lat) / (tile.toBBox.lowerRight.lat - tile.toBBox.upperLeft.lat)).toInt

      def lon2pix(l: Double) = math.round(ImageBBox.TILE_SIZE * (l - tile.toBBox.upperLeft.lon) / (tile.toBBox.lowerRight.lon - tile.toBBox.upperLeft.lon)).toInt

      //7println(s"tile=$tile")
      contours
        .filter(contour => isContourDepthForTile(tile.z, contour.level))
        .foreach(contour => {


          val lineWidth = if (contour.level.toInt % 10 == 0) 2 else 1
          g.setStroke(new BasicStroke(lineWidth))
          //println(s"contour=$contour")
          contour.units
            .filter(u => u.toBBox.isIntersect(tile.toBBox))
            .map(u => {
              //println(s"u=$u")
              val xFrom = lon2pix(u.lonFrom)
              val xTo = lon2pix(u.lonTo)
              val yFrom = lat2pix(u.latFrom)
              val yTo = lat2pix(u.latTo)
              (xFrom, yFrom, xTo, yTo)
            })
            .distinct
            .foreach({ case (xFrom, yFrom, xTo, yTo) => {
              g.drawLine(xFrom, yFrom, xTo, yTo)
            }

            })
        })

      g.dispose()
      FileUtils.forceMkdir(tile.toDir(dir))
      ImageIO.write(canvas, "png", tile.toFile(dir))
    }

  )


}
