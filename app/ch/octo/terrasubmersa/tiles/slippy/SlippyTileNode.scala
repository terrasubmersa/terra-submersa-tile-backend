package ch.octo.terrasubmersa.tiles.slippy

import ch.octo.terrasubmersa.coordinates.HasLatLonBBox

/**
  * A tile node, or in fact a Tile tree
  * Created by alex on 22.08.17.
  */
case class SlippyTileNode(current: SlippyTile, children: Set[SlippyTileNode]) {
  def walkDepthFirst(fApply: (SlippyTile) => Unit): Unit = {
    fApply(current)
    children.foreach((stn) => {
      stn.walkDepthFirst(fApply)
    })
  }
}

object SlippyTileNode {
  /**
    * build a list of sub tiles down to a level number.
    * The tiles cans eventually be filtered
    *
    * @param root       the root node
    * @param maxZoom    the maximum
    * @param filterTile a funtion to filter the tiles
    * @return
    */
  def generateTree(root: SlippyTile,
                   maxZoom: Int,
                   filterTile: (HasLatLonBBox) => Boolean = (h) => true
                  ): SlippyTileNode =
    if (root.z >= maxZoom) {
      SlippyTileNode(root, Set.empty)
    } else {
      SlippyTileNode(root,
        root.subTiles
          .filter(filterTile)
          .map(st => generateTree(st, maxZoom, filterTile))
      )
    }

  def walkAndDo(root: SlippyTile,
                maxZoom: Int,
                filterTile: (HasLatLonBBox) => Boolean = (h) => true,
                actionTile: (SlippyTile) => Unit = (tile) => None
               ): Unit =
    if (root.z <= maxZoom) {
      actionTile(root)
      root.subTiles
        .filter(filterTile)
        .foreach(st => {
          walkAndDo(st, maxZoom, filterTile, actionTile)
        })

    }

}