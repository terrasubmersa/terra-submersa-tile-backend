package ch.octo.terrasubmersa

/**
  * Created by alex on 21.08.17.
  */
package object slippy {
  case class UndefinedSlippyTileException(message:String) extends Exception(message)
}
