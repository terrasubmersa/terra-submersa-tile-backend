package ch.octo.terrasubmersa.tiles.services

import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords}
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemBoundaries}
import com.typesafe.config.ConfigFactory
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.api._
import reactivemongo.api.commands.bson.DefaultBSONCommandError
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter, Macros}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * write and reads data to/from MongoDB database
  * connection parameters are defined in the application.conf
  *
  * @TODO wire back connection if the contact was lost
  */
object TileMongoService {
  implicit def LatLonCoordsWriter: BSONDocumentWriter[LatLonCoords] = Macros.writer[LatLonCoords]

  implicit def LatLonBBoxWriter: BSONDocumentWriter[LatLonBBox] = Macros.writer[LatLonBBox]

  implicit def TileSystemBoundariesWriter: BSONDocumentWriter[TileSystemBoundaries] = Macros.writer[TileSystemBoundaries]

  implicit def TileSystemWriter: BSONDocumentWriter[TileSystem] = Macros.writer[TileSystem]

  implicit def LatLonCoordsReader: BSONDocumentReader[LatLonCoords] = Macros.reader[LatLonCoords]

  implicit def LatLonBBoxReader: BSONDocumentReader[LatLonBBox] = Macros.reader[LatLonBBox]

  implicit def TileSystemBoundariesReader: BSONDocumentReader[TileSystemBoundaries] = Macros.reader[TileSystemBoundaries]

  implicit def TileSystemReader: BSONDocumentReader[TileSystem] = Macros.reader[TileSystem]

  val mongoUri = ConfigFactory.load.getString("mongodb.uri")
  val mongoDatabaseName = ConfigFactory.load.getString("mongodb.databaseName")

  lazy val connection: Future[MongoConnection] = {
    val driver = MongoDriver()
    val parsedUri = MongoConnection.parseURI(mongoUri)
    Future.fromTry(parsedUri.map(driver.connection(_)))
  }

  /**
    * get the collection.
    * and eventually create indexes
    */
  lazy val tileSystemMongoCollection: Future[BSONCollection] = {
    connection
      .flatMap((c: MongoConnection) =>
        c.database(mongoDatabaseName)
          .map(x =>
            x.collection("tile_system")
          )
      )
      .flatMap((collection: BSONCollection) => setupCollection(collection))

  }

  /**
    * Setup indexes for the tile system collection
    *
    * @param collection
    * @return
    */
  def setupCollection(collection: BSONCollection): Future[BSONCollection] = {
    (collection.create().
      map { _ => collection }
      recover {
      //with the magic 48 number??? https://github.com/mongodb/mongo/blob/master/src/mongo/base/error_codes.err
      //NamespaceExists
      case e: DefaultBSONCommandError if e.code == Some(48) =>
        collection
    }
      )
      .flatMap(_ => {
        collection.indexesManager.ensure(Index(
          Seq("id" -> IndexType.Ascending), unique = true)
        )
          .map(_ => collection)
      })
  }

  /**
    * add a TileSystem into the
    *
    * @param tileSystem
    * @return
    */
  def addSystem(tileSystem: TileSystem): Future[Unit] = {
    val writeRes = tileSystemMongoCollection.flatMap(c => c.insert(tileSystem))
    writeRes.onComplete {
      case Failure(e) => throw e
      case Success(writeResult) =>
        println(s"successfully inserted document: $writeResult")
    }
    writeRes.map(_ => {})
  }

  /**
    *
    * @return
    */
  def systemList: Future[List[TileSystem]] = {
    val query = BSONDocument()
    tileSystemMongoCollection
      .flatMap(_.find(query)
        .cursor[TileSystem](ReadPreference.primaryPreferred)
        .collect[List](Int.MaxValue, Cursor.FailOnError[List[TileSystem]]())
      )
  }

  /**
    * add a TileSystem into the
    *
    * @param id the Tile id
    * @return
    */
  def removeSystem(id: String): Future[Unit] = {
    val selector = BSONDocument("id" -> id)

    val futureRemove = tileSystemMongoCollection.flatMap(c => c.remove(selector))
    futureRemove.onComplete {
      case Failure(e) => throw e
      case Success(writeResult) => println("successfully removed document: "+writeResult)
    }
    futureRemove.map(_ => {})
  }
}