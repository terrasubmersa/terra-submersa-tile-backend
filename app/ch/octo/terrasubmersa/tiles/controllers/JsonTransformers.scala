package ch.octo.terrasubmersa.tiles.controllers

import ch.octo.terrasubmersa.coordinates.{LatLonBBox, LatLonCoords}
import ch.octo.terrasubmersa.tiles.models.{TileSystem, TileSystemBoundaries}
import play.api.libs.json.{Format, JsPath, Reads, Writes}
import play.api.libs.functional.syntax._

object JsonTransformers {
  val LatLonCoordsReads: Reads[LatLonCoords] = (
    (JsPath \ "lat").read[Double] and
      (JsPath \ "lon").read[Double]

    ) (LatLonCoords.apply _)

  val LatLonCoordsWrites: Writes[LatLonCoords] = (
    (JsPath \ "lat").write[Double] and
      (JsPath \ "lon").write[Double]
    ) (unlift(LatLonCoords.unapply))

  implicit val LatLonCoordsFormat: Format[LatLonCoords] =
    Format(LatLonCoordsReads, LatLonCoordsWrites)

  val LatLonBBoxReads: Reads[LatLonBBox] = (
    (JsPath \ "upperLeft").read[LatLonCoords] and
      (JsPath \ "lowerRight").read[LatLonCoords]

    ) (LatLonBBox.apply _)

  val LatLonBBoxWrites: Writes[LatLonBBox] = (
    (JsPath \ "upperLeft").write[LatLonCoords] and
      (JsPath \ "lowerRight").write[LatLonCoords]
    ) (unlift(LatLonBBox.unapply))

  implicit val LatLonBBoxFormat: Format[LatLonBBox] =
    Format(LatLonBBoxReads, LatLonBBoxWrites)


  val TileSystemBoundariesReads: Reads[TileSystemBoundaries] = (
    (JsPath \ "minLevel").read[Int] and
      (JsPath \ "maxLevel").read[Int] and
      (JsPath \ "bbox").read[LatLonBBox]

    ) (TileSystemBoundaries.apply _)

  val TileSystemBoundariesWrites: Writes[TileSystemBoundaries] = (
    (JsPath \ "minLevel").write[Int] and
      (JsPath \ "maxLevel").write[Int] and
      (JsPath \ "bbox").write[LatLonBBox]
    ) (unlift(TileSystemBoundaries.unapply))

  implicit val TileSystemBoundariesFormat: Format[TileSystemBoundaries] =
    Format(TileSystemBoundariesReads, TileSystemBoundariesWrites)


  val TileSystemReads: Reads[TileSystem] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "boundaries").read[TileSystemBoundaries] and
      (JsPath \ "urlMask").read[String] and
      (JsPath \ "description").read[String] and
      (JsPath \ "copyright").read[String]

    ) (TileSystem.apply _)

  val TileSystemWrites: Writes[TileSystem] = (
    (JsPath \ "id").write[String] and
      (JsPath \ "boundaries").write[TileSystemBoundaries] and
      (JsPath \ "urlMask").write[String] and
      (JsPath \ "description").write[String] and
      (JsPath \ "copyright").write[String]
    ) (unlift(TileSystem.unapply))

  implicit val TileSystemFormat: Format[TileSystem] =
    Format(TileSystemReads, TileSystemWrites)
}
