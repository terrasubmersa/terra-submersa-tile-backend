package ch.octo.terrasubmersa.tiles.controllers

import javax.inject._

import play.api.http.DefaultHttpErrorHandler
import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import play.api.routing.Router
import scala.concurrent._

@Singleton
class ProdErrorHandler @Inject() (
                               env: Environment,
                               config: Configuration,
                               sourceMapper: OptionalSourceMapper,
                               router: Provider[Router]
                             ) extends DefaultHttpErrorHandler(env, config, sourceMapper, router) {


  override def onProdServerError(request: RequestHeader, exception: UsefulException) = {
    Logger.error("exception.getMessage", exception)
    Future.successful(
      InternalServerError("A server error occurred: " + exception.getMessage)
    )
  }
  override def onForbidden(request: RequestHeader, message: String) = {
    Future.successful(
      Forbidden("You're not allowed to access this resource.")
    )
  }

}