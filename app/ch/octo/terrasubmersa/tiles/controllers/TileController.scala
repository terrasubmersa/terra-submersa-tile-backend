package ch.octo.terrasubmersa.tiles.controllers

import javax.inject._

import ch.octo.terrasubmersa.tiles.controllers.JsonTransformers._
import ch.octo.terrasubmersa.tiles.models.TileSystem
import ch.octo.terrasubmersa.tiles.services.TileMongoService
import play.api.libs.json.{JsError, Json}
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

@Singleton
class TileController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
    * Create an Action to render an HTML page.
    *
    * The configuration in the `routes` file means that this method
    * will be called when the application receives a `GET` request with
    * a path of `/`.
    */
  def systemsList = Action.async {
    TileMongoService.systemList.map(x => Json.toJson(x))
      .map(l=>Ok(l))
  }

  /**
    * just posting a tile system description
    * @return
    */
  def addLegacySystem = Action.async(parse.json) { request =>
    val placeResult = request.body.validate[TileSystem]
    placeResult.fold(
      errors => {
        Future.successful(BadRequest(Json.obj("status" -> "KO", "message" -> JsError.toJson(errors))))
      },
      tileSystem => {
        TileMongoService.addSystem(tileSystem)
          .map( _ => Ok(Json.obj("status" -> "OK", "message" -> ("system '" + tileSystem.id + "' saved."))))
      }
    )
  }

  def removeSystem(id:String) =  Action.async { request =>
    TileMongoService.removeSystem(id).map(_ => Ok(s"remove tile system [$id]"))
  }
}
