package ch.octo.terrasubmersa.tiles.image

/**
  * all what is need to transfo one coordiante dimension into another one, relative to a tile
  * this will apply to latitude/height and longitude/width
  * Created by alex on 23.08.17.
  */
case class ImageTransfo(crop0: Int, crop1: Int, scale: Double, pad0: Int, pad1: Int) {
  override def equals(obj: scala.Any): Boolean = obj match {
    case t: ImageTransfo =>
      crop0 == t.crop0 &&
        crop1 == t.crop1 &&
        ((scale == t.scale) || math.abs((scale - t.scale) / (scale + t.scale)) < 0.001) &&
        pad0 == t.pad0 &&
        pad1 == t.pad1
    case _ => false
  }

}

//

object ImageTransfo {
  /**
    * Build the linear transformation, which consumes the less possible memory (no demential scaling)
    *
    * @param tileU0 tile coord lower bound (lat/lon)
    * @param tileU1 tile coord upper bound
    * @param imageU0 global image lower bound
    * @param imageU1 global image upper bound
    * @param tileWidth tile PNG size (height/width)
    * @param imageWidth global image PNG size
    * @return
    */
  def build(tileU0: Double, tileU1: Double, imageU0: Double, imageU1: Double, tileWidth: Int, imageWidth: Int): ImageTransfo = {
    val tileDeltaU = tileU1 - tileU0
    val imageDeltaU = imageU1 - imageU0

    val crop0 = if (imageU0 >= tileU0) 0 else (tileU0 - imageU0) / imageDeltaU * imageWidth
    val crop1 = if (imageU1 <= tileU1) 0 else (imageU1 - tileU1) / imageDeltaU * imageWidth
    val scale = imageDeltaU / tileDeltaU * tileWidth / imageWidth
    val pad0 = if (imageU0 <= tileU0) 0 else tileWidth * (imageU0 - tileU0) / tileDeltaU
    val pad1 = if (imageU1 >= tileU1) 0 else tileWidth * (tileU1 - imageU1) / tileDeltaU

    ImageTransfo(Math.round(crop0).toInt, Math.round(crop1).toInt, scale, Math.round(pad0).toInt, Math.round(pad1).toInt)
  }
}