package ch.octo.terrasubmersa

/**
  * Created by alex on 09.09.17.
  */
package object bathymetry {
  case class BathymetryCell(oVa:Option[Double])
}
