package ch.octo.terrasubmersa.tiles.bathymetry

import ch.octo.terrasubmersa.bathymetry.BathymetryCell
import ch.octo.terrasubmersa.coordinates.{HasLatLonBBox, LatLonBBox, LatLonCoords}

/**
  * The method is shamelessly stolen from the simple contour algorithm presented at https://en.wikipedia.org/wiki/Marching_squares
  *
  * @TODO there is certainly a system to produce all levels at once (%1, for example)
  */

case class ContourUnit(latFrom: Double, lonFrom: Double, latTo: Double, lonTo: Double) extends HasLatLonBBox {
  lazy val toBBox = {
    val (latMax, latMin) = if (latFrom > latTo) (latFrom, latTo) else (latTo, latFrom)
    val (lonMax, lonMin) = if (lonFrom > lonTo) (lonFrom, lonTo) else (lonTo, lonFrom)
    LatLonBBox(LatLonCoords(latMax, lonMin), LatLonCoords(latMin, lonMax))
  }
}

case class Contour(units: List[ContourUnit], level: Double) {

}

object Contour {
  def getUnit(bcNW: BathymetryCell, bcNE: BathymetryCell, bcSW: BathymetryCell, bcSE: BathymetryCell, threshold: Double, boundaries: LatLonBBox): List[ContourUnit] = {
    val north = boundaries.upperLeft.lat
    val south = boundaries.lowerRight.lat
    val east = boundaries.lowerRight.lon
    val west = boundaries.upperLeft.lon
    val midLat = 0.5 * (north + south)
    val midLon = 0.5 * (east + west)
    (bcNW, bcNE, bcSE, bcSW) match {
      case (BathymetryCell(Some(vNW)), BathymetryCell(Some(vNE)), BathymetryCell(Some(vSE)), BathymetryCell(Some(vSW))) =>
        val bit =
          (if (vNW < threshold) 0 else 8) |
            (if (vNE < threshold) 0 else 4) |
            (if (vSE < threshold) 0 else 2) |
            (if (vSW < threshold) 0 else 1)
        bit match {
          case (0) | (15) => Nil
          case (1) | (14) => List(ContourUnit(midLat, west, south, midLon))
          case (2) | (13) => List(ContourUnit(south, midLon, midLat, east))
          case (3) | (12) => List(ContourUnit(midLat, west, midLat, east))
          case (4) | (11) => List(ContourUnit(north, midLon, midLat, east))
          case (5) => List(ContourUnit(midLat, west, north, midLon), ContourUnit(south, midLon, midLat, east))
          case (6) | (9) => List(ContourUnit(south, midLon, north, midLon))
          case (7) | (8) => List(ContourUnit(midLat, west, north, midLon))
          case (10) => List(ContourUnit(midLat, west, south, midLon), ContourUnit(north, midLon, midLat, east))

        }
      case _ => Nil
    }
  }

  def build(bathymetryGrid: BathymetryGrid,
            threshold: Double,
            pix2LatLonCoord: Option[(Int, Int) => LatLonCoords] = None
           ): Contour = {
    val pix2ll = pix2LatLonCoord match {
      case Some(f) => f
      case None => (irow: Int, icol: Int) => {
        LatLonCoords(
          bathymetryGrid.boundaries.upperLeft.lat - 1.0 * irow / bathymetryGrid.nrows * (bathymetryGrid.boundaries.upperLeft.lat - bathymetryGrid.boundaries.lowerRight.lat),
          bathymetryGrid.boundaries.upperLeft.lon + 1.0 * icol / bathymetryGrid.ncols * (bathymetryGrid.boundaries.lowerRight.lon - bathymetryGrid.boundaries.upperLeft.lon)
        )
      }
    }
    val units = bathymetryGrid.cells.sliding(2, 1).zipWithIndex
      .flatMap({ case (twolines, irow) => {

        val List(line0, line1) = twolines
        line0.sliding(2, 1)
          .zip(line1.sliding(2, 1))
          .zipWithIndex
          .flatMap({ case ((List(cell00, cell01), List(cell10, cell11)), icol) => {
            val upperLeft = pix2ll(irow, icol)
            val lowerRight = pix2ll(irow + 1, icol + 1)

            Contour.getUnit(cell00, cell01, cell10, cell11, threshold, LatLonBBox(upperLeft, lowerRight))
          }
          })
      }
      })
    Contour(units.toList, threshold)
  }
}