package ch.octo.terrasubmersa.tiles.io.writers

import java.awt.{BasicStroke, Color}
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import ch.octo.terrasubmersa.application.GenerateContourTiles.{contours, grid}
import ch.octo.terrasubmersa.tiles.bathymetry.{BathymetryGrid, Contour}
import ch.octo.terrasubmersa.coordinates.LatLonBBox
import ch.octo.terrasubmersa.tiles.image.ImageBBox

object BathymetryGridSnapshotWriter {
  def saveZebra(grid: BathymetryGrid, fileRoot: String) = {
    def minDepth = grid.minDepth

    def maxDepth = grid.maxDepth

    def nbIntervals = 100


    def fColorDepth(depth: Double) = {
      val i = (nbIntervals * (depth - minDepth) / (maxDepth - minDepth)).toInt
      if (i % 2 == 0) {
        new Color(255, 255, 255, 0)
      } else {
        val gray = 128 / nbIntervals * i
        new Color(gray, gray, gray, 170)
      }
    }

    val canvas = new BufferedImage(grid.ncols, grid.nrows, BufferedImage.TYPE_INT_ARGB)
    val g = canvas.createGraphics()
    g.setColor(Color.darkGray)
    for {
      (row, iRow) <- grid.cells.zipWithIndex
      (cell, iCol) <- row.zipWithIndex if cell.oVa.isDefined
    } {
      g.setColor(fColorDepth(cell.oVa.get))
      g.drawLine(iCol, iRow, iCol, iRow)
    }
    g.dispose()
    ImageIO.write(canvas, "png", new File(s"$fileRoot-zebra-snapshot.png"))
  }

  def saveContours(grid:BathymetryGrid, contours: List[Contour], fileRoot: String) = {
    val canvas = new BufferedImage(grid.ncols, grid.nrows, BufferedImage.TYPE_INT_ARGB)
    val canvasBoundaries = grid.boundaries
    val g = canvas.createGraphics()
    g.setColor(Color.black)
    val lineWidth = List(1.0*grid.ncols/1600, 1.0*grid.nrows/1000).min.round
    g.setStroke(new BasicStroke(lineWidth))

    def lat2pix(l: Double) = math.round(grid.nrows * (l - grid.boundaries.upperLeft.lat) / (grid.boundaries.lowerRight.lat - grid.boundaries.upperLeft.lat)).toInt

    def lon2pix(l: Double) = math.round(grid.ncols * (l - grid.boundaries.upperLeft.lon) / (grid.boundaries.lowerRight.lon - grid.boundaries.upperLeft.lon)).toInt

    //7println(s"tile=$tile")
    contours.foreach(contour => {
      //println(s"contour=$contour")
      contour.units
        .map(u => {
          //println(s"u=$u")
          val xFrom = lon2pix(u.lonFrom)
          val xTo = lon2pix(u.lonTo)
          val yFrom = lat2pix(u.latFrom)
          val yTo = lat2pix(u.latTo)
          (xFrom, yFrom, xTo, yTo)
        })
        .distinct
        .foreach({ case (xFrom, yFrom, xTo, yTo) => {
          g.drawLine(xFrom, yFrom, xTo, yTo)
        }
        })
    })

    g.dispose()
    ImageIO.write(canvas, "png", new File(s"$fileRoot-contours-snapshot.png"))
  }
}
