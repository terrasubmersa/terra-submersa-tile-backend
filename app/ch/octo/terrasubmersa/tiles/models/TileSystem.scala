package ch.octo.terrasubmersa.tiles.models

case class TileSystem(id: String,
                      boundaries: TileSystemBoundaries,
                      urlMask: String,
                      description: String,
                      copyright: String) {

}
