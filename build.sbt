name := "terra-submersa-tile-backend"

version := "0.0.1-SNAPSHOT"


scalaVersion := "2.12.3"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  guice,
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.8",
  "com.sksamuel.scrimage" %% "scrimage-io-extra" % "2.1.8",
  "commons-io" % "commons-io" % "2.5",
  "org.apache.commons" % "commons-math3" % "3.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
  "org.reactivemongo" % "reactivemongo_2.12" % "0.12.7"
)
