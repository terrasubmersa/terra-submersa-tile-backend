## Play

### Data interaction
#### pushing legacy tile system description
We just push the description, not the actual tiles files

    curl -d '{
      "id":"abc",
      "boundaries":{"minLevel":12,"maxLevel":19,"bbox":{"upperLeft" : {"lat":12.34, "lon":56.78 } ,"lowerRight" : {"lat":-12.34, "lon":156.78 } } },
      "urlMask":"http://aaa/{z}/{x}/{y}",
      "description": "WTF",
      "copyright": "paf"
    }' -H "Content-Type: application/json" -X POST -v http://localhost:9000/system-legacy
    
    
#Removing a tile

    curl -X DELETE http://localhost:9000/system/abc

### Dev 
`sbt 013.16` is the version used for development
#### run
With hot update

    sbt ~run
    
*NB*: Maybe you need to modify/save one file to get rid of the `"file not watched"` message (registered sbt/play bug) 

#### test

    sbt ~test
    
### Package
#### Dev
Build an image 

    sbt docker:publishLocal
    version=$(perl -p -e 'print "$1" if /^version\s:=\s*"(.+)".*/; undef $_' build.sbt )
    name=$(perl -p -e 'print "$1" if /^name\s:=\s*"(.+)".*/; undef $_' build.sbt )
    
    docker tag $name terra-submersa/$name
    docker tag $name:$version terra-submersa/$name:latest
    docker tag $name:$version terra-submersa/$name:$version

    

## Tools

To view a .asc file in the terminal, zoom it out to the maximum and enter:

    cat  ~/Downloads/koilada_portoheli/koilada_utm34n.asc | sed 's/-9999 / /g' | perl -pe 's/\-\d*([0-9]+)(?:,\d+)? /$1/g' |cut -c1-1600